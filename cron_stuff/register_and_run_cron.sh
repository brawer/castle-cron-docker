#!/bin/sh
set -e

env > /tmp/cron.env
envsubst '${CRON_SCHEDULE}' </root/crontab.template > /root/crontab
crontab /root/crontab
crond -f -l 0
