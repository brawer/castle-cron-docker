#!/bin/bash
set -ex

python fetch_layers.py --output_dir=/var/data/results

chown -R nginx:nginx /var/data/results
