FROM nginx:alpine

ADD nginx-data-serve/default-site.conf /etc/nginx/conf.d/default.conf
RUN mkdir -p /var/data/html && chown -R :nginx /var/data/html

RUN apk add --update --no-cache gettext wget openssl python sudo

ENV DOCKERIZE_VERSION v0.6.1
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz

ENV CRON_SCHEDULE='* * * * *'

RUN wget -O /root/fetch_layers.py https://github.com/brawer/playground/raw/master/osm/fetch_layers.py
RUN chmod 0777 /root/fetch_layers.py

ADD cron_stuff/run_fetch_layers_and_set_permissions.sh /root/run.sh
ADD cron_stuff/crontab.template /root/crontab.template
ADD cron_stuff/register_and_run_cron.sh /root/register_and_run_cron.sh
